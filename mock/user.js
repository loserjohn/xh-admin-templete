
const tokens = {
  admin: {
    token: 'admin-token'
  },
  editor: {
    token: 'editor-token'
  }
}

const users = {
  'admin-token': {
    roles: ['admin'],
    introduction: 'I am a super administrator',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Super Admin'
  },
  'editor-token': {
    roles: ['editor'],
    introduction: 'I am an editor',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Normal Editor'
  }
}

export default [
  // user login
  {
    url: '/Auth/GetToken',
    type: 'post',
    response: config => {
      const { username } = config.body
      const token = tokens[username]

      // mock error
      if (!token) {
        return {
          data: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiMTM5NTkxNDEzNDUiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9zaWQiOiI2YzUzMWVlZmUyMDQ0ODU5YTlmZmZmZDY3MDEyYTgxNyIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IltcIkNSTUFkbWluXCIsXCJNQVhBZG1pblwiXSIsIm5iZiI6IjE2Mzc5NDA3MjgiLCJleHAiOjE2NTExMTY3MjgsImlzcyI6InNreS5jb20iLCJhdWQiOiJza3kuY29tIn0.KDUKzk_v6qEcUpm4pcJ9YPFLoHlVuSkAr1uJsiExjjE",
          msg: "",
          result: 1,
        }
      }

      return {
        code: 20000,
        data: token
      }
    }
  },

  // get user info
  {
    url: '/Auth/GetInfo',
    type: 'get',
    response: config => {
      const { token } = config.query
      const info = users[token]

    //   // mock error
    //   if (!info) {
    //     return {
    //       code: 50008,
    //       message: 'Login failed, unable to get user details.'
    //     }
    //   }

      return (
        {
          "result": 1, 
          "msg": "66666666666666", 
          "data": {
              "name": "13959141344", 
              "avatar": "http://model.fjdmll.com/upload/hard/hard.jpg", 
              "menus":pages,
            //   "menus": [
            //       {
            //           "page_id": "2", 
            //           "page_parent": "", 
            //           "page_name": "账户管理", 
            //           "page_num": "MaxAccount", 
            //           "page_icon": "", 
            //           "page_url": "/account", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "3", 
            //           "page_parent": "2", 
            //           "page_name": "机构管理", 
            //           "page_num": "MaxOrg", 
            //           "page_icon": "", 
            //           "page_url": "/org/orgmanage", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "019bd0a7845b4eb1bf2e4fdcab30708d", 
            //           "page_parent": "8826b44585e8476383b1ac38f979c2af", 
            //           "page_name": "角色授权", 
            //           "page_num": "MaxCmsColumnRole", 
            //           "page_icon": "", 
            //           "page_url": "/cms/columnrole/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "1e04a37d09b1401bbfcc3f41693d7f6a", 
            //           "page_parent": "3", 
            //           "page_name": "职务管理", 
            //           "page_num": "MaxAccountPost", 
            //           "page_icon": "", 
            //           "page_url": "/org/post/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "26f0728330e94f749a1a48b81c35c614", 
            //           "page_parent": "8", 
            //           "page_name": "字典项管理", 
            //           "page_num": "MaxDictItem", 
            //           "page_icon": "", 
            //           "page_url": "/config/dictionaryitem/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "3b788f562b50a3cf08dc2399242e298f", 
            //           "page_parent": "5", 
            //           "page_name": "权限配置", 
            //           "page_num": "MaxUserRight", 
            //           "page_icon": "", 
            //           "page_url": "/account/userright/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "686cfdcd30a046e2be15b888ee47f1d7", 
            //           "page_parent": "21dbccb183014eb182414211fcf16a60", 
            //           "page_name": "生成测试", 
            //           "page_num": "TestConsumer", 
            //           "page_icon": "", 
            //           "page_url": "/webbuilding/testconsumer", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "79056a1d05a144259e10c0729e830393", 
            //           "page_parent": "7", 
            //           "page_name": "系统管理", 
            //           "page_num": "MaxSystem", 
            //           "page_icon": "", 
            //           "page_url": "/sys/system", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "8826b44585e8476383b1ac38f979c2af", 
            //           "page_parent": "913dadd571244b3eaf66149dd210aa76", 
            //           "page_name": "栏目管理", 
            //           "page_num": "MaxCmsColumn", 
            //           "page_icon": "", 
            //           "page_url": "/cms/column", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "bef362d5e29b45d9a330ec06dd6776e7", 
            //           "page_parent": "21dbccb183014eb182414211fcf16a60", 
            //           "page_name": "布局管理", 
            //           "page_num": "MaxBuildLayout", 
            //           "page_icon": "", 
            //           "page_url": "/webbuilding/layout", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "e93c23ad0ea1458e8fef5b9ea15fcdce", 
            //           "page_parent": "9", 
            //           "page_name": "表单项管理", 
            //           "page_num": "MaxFormItem", 
            //           "page_icon": "", 
            //           "page_url": "/config/formitem/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "7", 
            //           "page_parent": "", 
            //           "page_name": "系统设置", 
            //           "page_num": "MaxSys", 
            //           "page_icon": "", 
            //           "page_url": "/sys", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "8", 
            //           "page_parent": "6", 
            //           "page_name": "字典管理", 
            //           "page_num": "MaxDict", 
            //           "page_icon": "", 
            //           "page_url": "/config/dictionary", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "12", 
            //           "page_parent": "79056a1d05a144259e10c0729e830393", 
            //           "page_name": "模块管理", 
            //           "page_num": "MaxPage", 
            //           "page_icon": "", 
            //           "page_url": "/sys/page/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "1d5d7901f27449f0afac4afb5ca3053b", 
            //           "page_parent": "5", 
            //           "page_name": "IP管理", 
            //           "page_num": "MaxIp", 
            //           "page_icon": "", 
            //           "page_url": "/account/iplimit/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "70d5626c86534c0e9b8be58118c85146", 
            //           "page_parent": "21dbccb183014eb182414211fcf16a60", 
            //           "page_name": "模块管理", 
            //           "page_num": "MaxBuildModule", 
            //           "page_icon": "", 
            //           "page_url": "/webbuilding/module", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "7e1b99e26e0a49f0a6f66ea5f101ca0d", 
            //           "page_parent": "3", 
            //           "page_name": "功能授权", 
            //           "page_num": "MaxPostRight", 
            //           "page_icon": "", 
            //           "page_url": "/org/postright/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "ad9ea05ceb5748b7a74e84c5718147e5", 
            //           "page_parent": "8826b44585e8476383b1ac38f979c2af", 
            //           "page_name": "栏目授权", 
            //           "page_num": "MaxCmsRoleColumn", 
            //           "page_icon": "", 
            //           "page_url": "/cms/rolecolumn/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "6", 
            //           "page_parent": "", 
            //           "page_name": "配置管理", 
            //           "page_num": "MaxConfig", 
            //           "page_icon": "", 
            //           "page_url": "/config", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "9", 
            //           "page_parent": "6", 
            //           "page_name": "表单管理", 
            //           "page_num": "MaxForm", 
            //           "page_icon": "", 
            //           "page_url": "/config/form", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "60011e80638a4a6c9ec5b9b40a8b809c", 
            //           "page_parent": "21dbccb183014eb182414211fcf16a60", 
            //           "page_name": "页面管理", 
            //           "page_num": "MaxBuildPage", 
            //           "page_icon": "", 
            //           "page_url": "/webbuilding/page", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "745b08b55bc968c36f0a8f1ed170c57f", 
            //           "page_parent": "79056a1d05a144259e10c0729e830393", 
            //           "page_name": "角色管理", 
            //           "page_num": "MaxRole", 
            //           "page_icon": "", 
            //           "page_url": "/sys/role/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "90944fbcf7784a50a6f624a0dd4138d2", 
            //           "page_parent": "3", 
            //           "page_name": "角色分配", 
            //           "page_num": "MaxPostRole", 
            //           "page_icon": "", 
            //           "page_url": "/org/postrole/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "aabfa359b5371492f497688aea96ed36", 
            //           "page_parent": "5", 
            //           "page_name": "角色配置", 
            //           "page_num": "MaxUserRole", 
            //           "page_icon": "", 
            //           "page_url": "/account/userrole/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "fe4dcf051fc84e3e9f7ffa33f7cbbe5c", 
            //           "page_parent": "8826b44585e8476383b1ac38f979c2af", 
            //           "page_name": "职务授权", 
            //           "page_num": "MaxCmsColumnPost", 
            //           "page_icon": "", 
            //           "page_url": "/cms/columnpost/:id", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "5", 
            //           "page_parent": "2", 
            //           "page_name": "用户管理", 
            //           "page_num": "MaxUser", 
            //           "page_icon": "", 
            //           "page_url": "/account/user", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "10", 
            //           "page_parent": "6", 
            //           "page_name": "参数管理", 
            //           "page_num": "MaxSettings", 
            //           "page_icon": "", 
            //           "page_url": "/config/settings", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "4a1cffc61dcd496f85d1f60d0174ae6d", 
            //           "page_parent": "5", 
            //           "page_name": "积分查询", 
            //           "page_num": "MaxIntegral", 
            //           "page_icon": "", 
            //           "page_url": "/account/integral", 
            //           "page_type": "2"
            //       }, 
            //       {
            //           "page_id": "5ac2b4ea47fa410981ec71b104d20689", 
            //           "page_parent": "913dadd571244b3eaf66149dd210aa76", 
            //           "page_name": "资讯管理", 
            //           "page_num": "MaxCmsInfo", 
            //           "page_icon": "", 
            //           "page_url": "/cms/info?mode=manage&column=", 
            //           "page_type": "1"
            //       }, 
            //       {
            //           "page_id": "913dadd571244b3eaf66149dd210aa76", 
            //           "page_parent": "", 
            //           "page_name": "内容管理", 
            //           "page_num": "MaxCms", 
            //           "page_icon": "", 
            //           "page_url": "/cms", 
            //           "page_type": "1"
            //       }
            //   ], 
              "userInfo": {
                  "unit_name": "重大项目监控管理", 
                  "user_name": "13959141345", 
                  "user_this_time": "2021-11-26T23:16:34", 
                  "user_last_time": "2021-11-26T23:12:17", 
                  "user_full_name": "13959141345", 
                  "user_unit": "c7c678db1b70448eaebf06c86df691b0", 
                  "user_home": null, 
                  "integral": 1000
              }
          }
      }
      )
    }
  },

  // user logout
  {
    url: '/user/logout',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]
 
 

const pages = [
   
    {
        "id": "7", 
        "page_id": "7", 
        "page_num": "system",  //唯一标识  必须唯一
        "page_parent": "", 
        "parentId": "", 
        "page_name": "系统设置", 
        "page_icon": "", 
        // "page_url": "/sys", 
        // "page_ispublish": 1,   
        "page_isMenus": 1,      //是否菜单
        "page_ordernum": 2,     //排序
        "page_status": "1",     //是否启用
        "children": [ 
            {
                "id": "12", 
                "page_id": "12", 
                "page_num": "system_menus", 
                "page_parent": "7", 
                "parentId": "79056a1d05a144259e10c0729e830393", 
                "page_name": "菜单管理", 
                "page_icon": "", 
                // "page_url": "/sys/page", 
                // "page_ispublish": "0", 
                "page_isMenus": 1, 
                "page_ordernum": 2, 
                "page_status": "1", 
                "children": [ ]
            }, 
             {
                "id": "8c16a2464469482d8c200f3859aa7c14", 
                "page_id": "8c16a2464469482d8c200f3859aa7c14", 
                "page_num": "MaxSchedule", 
                "page_parent": "7", 
                "parentId": "7", 
                "page_name": "任务管理", 
                "page_icon": "", 
                // "page_url": "/quarzt/schedule", 
                // "page_ispublish": "0", 
                "page_type":1, 
                "page_ordernum": 6, 
                "page_status": "1", 
                "children": [ ]
            }, 
            // {
            //     "id": "b40f9f87a78b2b9dd98e5b0329249e82", 
            //     "page_id": "b40f9f87a78b2b9dd98e5b0329249e82", 
            //     "page_num": "MaxRoleRight", 
            //     "page_parent": "79056a1d05a144259e10c0729e830393", 
            //     "parentId": "79056a1d05a144259e10c0729e830393", 
            //     "page_name": "功能授权", 
            //     "page_icon": "", 
            //     "page_url": "/sys/roleright/:id", 
            //     "page_ispublish": "0", 
            //     "page_type": "模块", 
            //     "page_ordernum": 4, 
            //     "page_status": "1", 
            //     "children": [ ]
            // }, 
            // {
            //     "id": "c49ad5eb8dc54769b08d3721725d858a", 
            //     "page_id": "c49ad5eb8dc54769b08d3721725d858a", 
            //     "page_num": "MaxRoleUser", 
            //     "page_parent": "79056a1d05a144259e10c0729e830393", 
            //     "parentId": "79056a1d05a144259e10c0729e830393", 
            //     "page_name": "用户授权", 
            //     "page_icon": "", 
            //     "page_url": "/sys/roleuser/:id", 
            //     "page_ispublish": "0", 
            //     "page_type": "模块", 
            //     "page_ordernum": 5, 
            //     "page_status": "1", 
            //     "children": [ ]
            // },
        ]},
    //         {
    //             "id": "8c16a2464469482d8c200f3859aa7c14", 
    //             "page_id": "8c16a2464469482d8c200f3859aa7c14", 
    //             "page_num": "MaxSchedule", 
    //             "page_parent": "7", 
    //             "parentId": "7", 
    //             "page_name": "任务管理", 
    //             "page_icon": "", 
    //             "page_url": "/quarzt/schedule", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 6, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "c771e0d30d984917b796fdd7427b1f4e", 
    //             "page_id": "c771e0d30d984917b796fdd7427b1f4e", 
    //             "page_num": "MaxLogNlog", 
    //             "page_parent": "7", 
    //             "parentId": "7", 
    //             "page_name": "系统日志", 
    //             "page_icon": "", 
    //             "page_url": "/log/nlog", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 7, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "d7daa9e13173424aaae2467a40921b7e", 
    //             "page_id": "d7daa9e13173424aaae2467a40921b7e", 
    //             "page_num": "MaxLogApi", 
    //             "page_parent": "7", 
    //             "parentId": "7", 
    //             "page_name": "接口日志", 
    //             "page_icon": "", 
    //             "page_url": "/log/api", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 8, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "7e559dacf5d14006bb2e7b9b75850204", 
    //             "page_id": "7e559dacf5d14006bb2e7b9b75850204", 
    //             "page_num": "MaxLogError", 
    //             "page_parent": "7", 
    //             "parentId": "7", 
    //             "page_name": "异常日志", 
    //             "page_icon": "", 
    //             "page_url": "/log/error", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 9, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "b20054c8545a44339b536628d2af4093", 
    //             "page_id": "b20054c8545a44339b536628d2af4093", 
    //             "page_num": "MaxLogTrace", 
    //             "page_parent": "7", 
    //             "parentId": "7", 
    //             "page_name": "运行日志", 
    //             "page_icon": "", 
    //             "page_url": "/log/trace", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 10, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "90fb161100e74a03be5373773b848447", 
    //             "page_id": "90fb161100e74a03be5373773b848447", 
    //             "page_num": "MaxHelpEdit", 
    //             "page_parent": "7", 
    //             "parentId": "7", 
    //             "page_name": "帮助手册", 
    //             "page_icon": "", 
    //             "page_url": "/help/edit/:id/:title", 
    //             "page_ispublish": "0", 
    //             "page_type": "模块", 
    //             "page_ordernum": 11, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "d3640a1823ba47ada99a0d57e68c5c8e", 
    //             "page_id": "d3640a1823ba47ada99a0d57e68c5c8e", 
    //             "page_num": "MaxHelpList", 
    //             "page_parent": "7", 
    //             "parentId": "7", 
    //             "page_name": "帮助文档", 
    //             "page_icon": "", 
    //             "page_url": "/help/list/:id", 
    //             "page_ispublish": "0", 
    //             "page_type": "模块", 
    //             "page_ordernum": 12, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }
    //     ]
    // }, 
    // {
    //     "id": "6", 
    //     "page_id": "6", 
    //     "page_num": "MaxConfig", 
    //     "page_parent": "", 
    //     "parentId": "", 
    //     "page_name": "配置管理", 
    //     "page_icon": "", 
    //     "page_url": "/config", 
    //     "page_ispublish": "0", 
    //     "page_type": "菜单", 
    //     "page_ordernum": 3, 
    //     "page_status": "1", 
    //     "children": [
    //         {
    //             "id": "8", 
    //             "page_id": "8", 
    //             "page_num": "MaxDict", 
    //             "page_parent": "6", 
    //             "parentId": "6", 
    //             "page_name": "字典管理", 
    //             "page_icon": "", 
    //             "page_url": "/config/dictionary", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 2, 
    //             "page_status": "1", 
    //             "children": [
    //                 {
    //                     "id": "26f0728330e94f749a1a48b81c35c614", 
    //                     "page_id": "26f0728330e94f749a1a48b81c35c614", 
    //                     "page_num": "MaxDictItem", 
    //                     "page_parent": "8", 
    //                     "parentId": "8", 
    //                     "page_name": "字典项管理", 
    //                     "page_icon": "", 
    //                     "page_url": "/config/dictionaryitem/:id", 
    //                     "page_ispublish": "0", 
    //                     "page_type": "模块", 
    //                     "page_ordernum": 1, 
    //                     "page_status": "1", 
    //                     "children": [ ]
    //                 }
    //             ]
    //         }, 
    //         {
    //             "id": "9", 
    //             "page_id": "9", 
    //             "page_num": "MaxForm", 
    //             "page_parent": "6", 
    //             "parentId": "6", 
    //             "page_name": "表单管理", 
    //             "page_icon": "", 
    //             "page_url": "/config/form", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 3, 
    //             "page_status": "1", 
    //             "children": [
    //                 {
    //                     "id": "e93c23ad0ea1458e8fef5b9ea15fcdce", 
    //                     "page_id": "e93c23ad0ea1458e8fef5b9ea15fcdce", 
    //                     "page_num": "MaxFormItem", 
    //                     "page_parent": "9", 
    //                     "parentId": "9", 
    //                     "page_name": "表单项管理", 
    //                     "page_icon": "", 
    //                     "page_url": "/config/formitem/:id", 
    //                     "page_ispublish": "0", 
    //                     "page_type": "模块", 
    //                     "page_ordernum": 1, 
    //                     "page_status": "1", 
    //                     "children": [ ]
    //                 }
    //             ]
    //         }, 
    //         {
    //             "id": "10", 
    //             "page_id": "10", 
    //             "page_num": "MaxSettings", 
    //             "page_parent": "6", 
    //             "parentId": "6", 
    //             "page_name": "参数管理", 
    //             "page_icon": "", 
    //             "page_url": "/config/settings", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 4, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "21363c57c08527bf8309cecd0e229781", 
    //             "page_id": "21363c57c08527bf8309cecd0e229781", 
    //             "page_num": "MaxSetting", 
    //             "page_parent": "6", 
    //             "parentId": "6", 
    //             "page_name": "参数配置", 
    //             "page_icon": "", 
    //             "page_url": "/config/setting", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 5, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "4e249504353710a139d248cff2da09b3", 
    //             "page_id": "4e249504353710a139d248cff2da09b3", 
    //             "page_num": "MaxLanguage", 
    //             "page_parent": "6", 
    //             "parentId": "6", 
    //             "page_name": "语种管理", 
    //             "page_icon": "", 
    //             "page_url": "/config/language", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 6, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "9505d0aba01a4e7ba38e7c3084fe71bf", 
    //             "page_id": "9505d0aba01a4e7ba38e7c3084fe71bf", 
    //             "page_num": "RealtimeCache", 
    //             "page_parent": "6", 
    //             "parentId": "6", 
    //             "page_name": "实时缓存", 
    //             "page_icon": "", 
    //             "page_url": "/cache/realtime", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 7, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }
    //     ]
    // }, 
    // {
    //     "id": "913dadd571244b3eaf66149dd210aa76", 
    //     "page_id": "913dadd571244b3eaf66149dd210aa76", 
    //     "page_num": "MaxCms", 
    //     "page_parent": "", 
    //     "parentId": "", 
    //     "page_name": "内容管理", 
    //     "page_icon": "", 
    //     "page_url": "/cms", 
    //     "page_ispublish": "0", 
    //     "page_type": "菜单", 
    //     "page_ordernum": 4, 
    //     "page_status": "1", 
    //     "children": [
    //         {
    //             "id": "8826b44585e8476383b1ac38f979c2af", 
    //             "page_id": "8826b44585e8476383b1ac38f979c2af", 
    //             "page_num": "MaxCmsColumn", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "栏目管理", 
    //             "page_icon": "", 
    //             "page_url": "/cms/column", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 1, 
    //             "page_status": "1", 
    //             "children": [
    //                 {
    //                     "id": "019bd0a7845b4eb1bf2e4fdcab30708d", 
    //                     "page_id": "019bd0a7845b4eb1bf2e4fdcab30708d", 
    //                     "page_num": "MaxCmsColumnRole", 
    //                     "page_parent": "8826b44585e8476383b1ac38f979c2af", 
    //                     "parentId": "8826b44585e8476383b1ac38f979c2af", 
    //                     "page_name": "角色授权", 
    //                     "page_icon": "", 
    //                     "page_url": "/cms/columnrole/:id", 
    //                     "page_ispublish": "0", 
    //                     "page_type": "模块", 
    //                     "page_ordernum": 1, 
    //                     "page_status": "1", 
    //                     "children": [ ]
    //                 }, 
    //                 {
    //                     "id": "ad9ea05ceb5748b7a74e84c5718147e5", 
    //                     "page_id": "ad9ea05ceb5748b7a74e84c5718147e5", 
    //                     "page_num": "MaxCmsRoleColumn", 
    //                     "page_parent": "8826b44585e8476383b1ac38f979c2af", 
    //                     "parentId": "8826b44585e8476383b1ac38f979c2af", 
    //                     "page_name": "栏目授权", 
    //                     "page_icon": "", 
    //                     "page_url": "/cms/rolecolumn/:id", 
    //                     "page_ispublish": "0", 
    //                     "page_type": "模块", 
    //                     "page_ordernum": 2, 
    //                     "page_status": "1", 
    //                     "children": [ ]
    //                 }, 
    //                 {
    //                     "id": "fe4dcf051fc84e3e9f7ffa33f7cbbe5c", 
    //                     "page_id": "fe4dcf051fc84e3e9f7ffa33f7cbbe5c", 
    //                     "page_num": "MaxCmsColumnPost", 
    //                     "page_parent": "8826b44585e8476383b1ac38f979c2af", 
    //                     "parentId": "8826b44585e8476383b1ac38f979c2af", 
    //                     "page_name": "职务授权", 
    //                     "page_icon": "", 
    //                     "page_url": "/cms/columnpost/:id", 
    //                     "page_ispublish": "0", 
    //                     "page_type": "模块", 
    //                     "page_ordernum": 3, 
    //                     "page_status": "1", 
    //                     "children": [ ]
    //                 }
    //             ]
    //         }, 
    //         {
    //             "id": "5ac2b4ea47fa410981ec71b104d20689", 
    //             "page_id": "5ac2b4ea47fa410981ec71b104d20689", 
    //             "page_num": "MaxCmsInfo", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "资讯管理", 
    //             "page_icon": "", 
    //             "page_url": "/cms/info?mode=manage&column=", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 4, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "5ac2b4ea47fa410981ec71b104d20690", 
    //             "page_id": "5ac2b4ea47fa410981ec71b104d20690", 
    //             "page_num": "MaxCmsCheck", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "资讯审核", 
    //             "page_icon": "", 
    //             "page_url": "/cms/check>>/cms/info?mode=check&column=", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 5, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "5ac2b4ea47fa410981ec71b104d20691", 
    //             "page_id": "5ac2b4ea47fa410981ec71b104d20691", 
    //             "page_num": "MaxCmsPublish", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "资讯发布", 
    //             "page_icon": "", 
    //             "page_url": "/cms/publish>>/cms/info?mode=publish&column=", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 6, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "5ac2b4ea47fa410981ec71b104d20692", 
    //             "page_id": "5ac2b4ea47fa410981ec71b104d20692", 
    //             "page_num": "MaxCmsView", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "资讯信息", 
    //             "page_icon": "", 
    //             "page_url": "/cms/view>>/cms/info?mode=view&column=", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 7, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "d47861073e284adbaa03990945fa996d", 
    //             "page_id": "d47861073e284adbaa03990945fa996d", 
    //             "page_num": "MaxCmsEdit", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "资讯编辑", 
    //             "page_icon": "", 
    //             "page_url": "/cms/edit/:id/:mode/:column", 
    //             "page_ispublish": "0", 
    //             "page_type": "模块", 
    //             "page_ordernum": 8, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "d8de6f37a9a2413a806e6880801f195b", 
    //             "page_id": "d8de6f37a9a2413a806e6880801f195b", 
    //             "page_num": "MaxCmsDetail", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "资讯浏览", 
    //             "page_icon": "", 
    //             "page_url": "/cms/view/:id", 
    //             "page_ispublish": "0", 
    //             "page_type": "模块", 
    //             "page_ordernum": 9, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "b6e1da6e067e43ef9de9d75c24c67af6", 
    //             "page_id": "b6e1da6e067e43ef9de9d75c24c67af6", 
    //             "page_num": "MaxNavigation", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "导航管理", 
    //             "page_icon": "", 
    //             "page_url": "/cms/navigation", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 10, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "ea37d3d027da4c91b2c3dd20cf93d640", 
    //             "page_id": "ea37d3d027da4c91b2c3dd20cf93d640", 
    //             "page_num": "MaxLink", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "链接管理", 
    //             "page_icon": "", 
    //             "page_url": "/cms/link", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 12, 
    //             "page_status": "1", 
    //             "children": [
    //                 {
    //                     "id": "dddb101694cf44c6b7ce18f7a283e8ea", 
    //                     "page_id": "dddb101694cf44c6b7ce18f7a283e8ea", 
    //                     "page_num": "MaxLinkType", 
    //                     "page_parent": "ea37d3d027da4c91b2c3dd20cf93d640", 
    //                     "parentId": "ea37d3d027da4c91b2c3dd20cf93d640", 
    //                     "page_name": "链接类型管理", 
    //                     "page_icon": "", 
    //                     "page_url": "/cms/linktype", 
    //                     "page_ispublish": "0", 
    //                     "page_type": "模块", 
    //                     "page_ordernum": 11, 
    //                     "page_status": "1", 
    //                     "children": [ ]
    //                 }
    //             ]
    //         }, 
    //         {
    //             "id": "9d3e315f34da4fba881e26d2b33d4845", 
    //             "page_id": "9d3e315f34da4fba881e26d2b33d4845", 
    //             "page_num": "MaxCons", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "咨询管理", 
    //             "page_icon": "", 
    //             "page_url": "/cms/consultation", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 13, 
    //             "page_status": "1", 
    //             "children": [
    //                 {
    //                     "id": "d41b06aece4642aeab6788930808acfe", 
    //                     "page_id": "d41b06aece4642aeab6788930808acfe", 
    //                     "page_num": "MaxConsType", 
    //                     "page_parent": "9d3e315f34da4fba881e26d2b33d4845", 
    //                     "parentId": "9d3e315f34da4fba881e26d2b33d4845", 
    //                     "page_name": "咨询类型管理", 
    //                     "page_icon": "", 
    //                     "page_url": "/cms/consultationtype", 
    //                     "page_ispublish": "0", 
    //                     "page_type": "模块", 
    //                     "page_ordernum": 14, 
    //                     "page_status": "1", 
    //                     "children": [ ]
    //                 }
    //             ]
    //         }, 
    //         {
    //             "id": "812a14e3265149e78e44eedff15ef057", 
    //             "page_id": "812a14e3265149e78e44eedff15ef057", 
    //             "page_num": "MaxDown", 
    //             "page_parent": "913dadd571244b3eaf66149dd210aa76", 
    //             "parentId": "913dadd571244b3eaf66149dd210aa76", 
    //             "page_name": "下载管理", 
    //             "page_icon": "", 
    //             "page_url": "/cms/download", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 15, 
    //             "page_status": "1", 
    //             "children": [
    //                 {
    //                     "id": "8a952b772d0e43b98ef221d0209f4a98", 
    //                     "page_id": "8a952b772d0e43b98ef221d0209f4a98", 
    //                     "page_num": "MaxDownType", 
    //                     "page_parent": "812a14e3265149e78e44eedff15ef057", 
    //                     "parentId": "812a14e3265149e78e44eedff15ef057", 
    //                     "page_name": "下载类型管理", 
    //                     "page_icon": "", 
    //                     "page_url": "/cms/downloadtype", 
    //                     "page_ispublish": "0", 
    //                     "page_type": "模块", 
    //                     "page_ordernum": 16, 
    //                     "page_status": "1", 
    //                     "children": [ ]
    //                 }
    //             ]
    //         }
    //     ]
    // }, 
    // {
    //     "id": "21dbccb183014eb182414211fcf16a60", 
    //     "page_id": "21dbccb183014eb182414211fcf16a60", 
    //     "page_num": "MaxBuild", 
    //     "page_parent": "", 
    //     "parentId": "", 
    //     "page_name": "建站管理", 
    //     "page_icon": "", 
    //     "page_url": "/webbuilding", 
    //     "page_ispublish": "0", 
    //     "page_type": "菜单", 
    //     "page_ordernum": 5, 
    //     "page_status": "1", 
    //     "children": [
    //         {
    //             "id": "686cfdcd30a046e2be15b888ee47f1d7", 
    //             "page_id": "686cfdcd30a046e2be15b888ee47f1d7", 
    //             "page_num": "TestConsumer", 
    //             "page_parent": "21dbccb183014eb182414211fcf16a60", 
    //             "parentId": "21dbccb183014eb182414211fcf16a60", 
    //             "page_name": "生成测试", 
    //             "page_icon": "", 
    //             "page_url": "/webbuilding/testconsumer", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 1, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "bef362d5e29b45d9a330ec06dd6776e7", 
    //             "page_id": "bef362d5e29b45d9a330ec06dd6776e7", 
    //             "page_num": "MaxBuildLayout", 
    //             "page_parent": "21dbccb183014eb182414211fcf16a60", 
    //             "parentId": "21dbccb183014eb182414211fcf16a60", 
    //             "page_name": "布局管理", 
    //             "page_icon": "", 
    //             "page_url": "/webbuilding/layout", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 1, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "70d5626c86534c0e9b8be58118c85146", 
    //             "page_id": "70d5626c86534c0e9b8be58118c85146", 
    //             "page_num": "MaxBuildModule", 
    //             "page_parent": "21dbccb183014eb182414211fcf16a60", 
    //             "parentId": "21dbccb183014eb182414211fcf16a60", 
    //             "page_name": "模块管理", 
    //             "page_icon": "", 
    //             "page_url": "/webbuilding/module", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 2, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "60011e80638a4a6c9ec5b9b40a8b809c", 
    //             "page_id": "60011e80638a4a6c9ec5b9b40a8b809c", 
    //             "page_num": "MaxBuildPage", 
    //             "page_parent": "21dbccb183014eb182414211fcf16a60", 
    //             "parentId": "21dbccb183014eb182414211fcf16a60", 
    //             "page_name": "页面管理", 
    //             "page_icon": "", 
    //             "page_url": "/webbuilding/page", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 3, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }
    //     ]
    // }, 
    // {
    //     "id": "bac188a3cf2d457da5232f71641819d8", 
    //     "page_id": "bac188a3cf2d457da5232f71641819d8", 
    //     "page_num": "Business", 
    //     "page_parent": "", 
    //     "parentId": "", 
    //     "page_name": "交易管理", 
    //     "page_icon": "", 
    //     "page_url": "/business", 
    //     "page_ispublish": "0", 
    //     "page_type": "菜单", 
    //     "page_ordernum": 5, 
    //     "page_status": "1", 
    //     "children": [
    //         {
    //             "id": "66f75bfa9c3148d7adda987aec8a9080", 
    //             "page_id": "66f75bfa9c3148d7adda987aec8a9080", 
    //             "page_num": "OrderManage", 
    //             "page_parent": "bac188a3cf2d457da5232f71641819d8", 
    //             "parentId": "bac188a3cf2d457da5232f71641819d8", 
    //             "page_name": "订单管理", 
    //             "page_icon": "", 
    //             "page_url": "/Shop/ShopOrder", 
    //             "page_ispublish": "1", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 1, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "f9ec2f3bed0746e8befc949e0af04622", 
    //             "page_id": "f9ec2f3bed0746e8befc949e0af04622", 
    //             "page_num": "CashManage", 
    //             "page_parent": "bac188a3cf2d457da5232f71641819d8", 
    //             "parentId": "bac188a3cf2d457da5232f71641819d8", 
    //             "page_name": "提现管理", 
    //             "page_icon": "", 
    //             "page_url": "/Shop/ShopCashOut", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 1, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "f21d3d49f30b47359817ff70c487b234", 
    //             "page_id": "f21d3d49f30b47359817ff70c487b234", 
    //             "page_num": "Refund", 
    //             "page_parent": "bac188a3cf2d457da5232f71641819d8", 
    //             "parentId": "bac188a3cf2d457da5232f71641819d8", 
    //             "page_name": "退款管理", 
    //             "page_icon": "", 
    //             "page_url": "/Shop/ShopRefund", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 2, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "2dd9a66717fe4d16b4df75582eb1a86c", 
    //             "page_id": "2dd9a66717fe4d16b4df75582eb1a86c", 
    //             "page_num": "RefungGoods", 
    //             "page_parent": "bac188a3cf2d457da5232f71641819d8", 
    //             "parentId": "bac188a3cf2d457da5232f71641819d8", 
    //             "page_name": "退货管理", 
    //             "page_icon": "", 
    //             "page_url": "/Shop/ShopRefundGoods", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 3, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "95d86abc3d9f4d9ea26cca1f47a4bca3", 
    //             "page_id": "95d86abc3d9f4d9ea26cca1f47a4bca3", 
    //             "page_num": "EmsSet", 
    //             "page_parent": "bac188a3cf2d457da5232f71641819d8", 
    //             "parentId": "bac188a3cf2d457da5232f71641819d8", 
    //             "page_name": "邮费模板", 
    //             "page_icon": "", 
    //             "page_url": "/Shop/ShopEmsSet", 
    //             "page_ispublish": "0", 
    //             "page_type": "菜单", 
    //             "page_ordernum": 4, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }
    //     ]
    // }, 
    // {
    //     "id": "2b290c31df96418fa9ed4d67f7a174af", 
    //     "page_id": "2b290c31df96418fa9ed4d67f7a174af", 
    //     "page_num": "MaxCode", 
    //     "page_parent": "", 
    //     "parentId": "", 
    //     "page_name": "代码生成", 
    //     "page_icon": "", 
    //     "page_url": "/coding", 
    //     "page_ispublish": "0", 
    //     "page_type": "模块", 
    //     "page_ordernum": 6, 
    //     "page_status": "1", 
    //     "children": [
    //         {
    //             "id": "7bcf84e014dc457e96a5dabd8e51a1ef", 
    //             "page_id": "7bcf84e014dc457e96a5dabd8e51a1ef", 
    //             "page_num": "MaxCodeModel", 
    //             "page_parent": "2b290c31df96418fa9ed4d67f7a174af", 
    //             "parentId": "2b290c31df96418fa9ed4d67f7a174af", 
    //             "page_name": "数据模型生成", 
    //             "page_icon": "", 
    //             "page_url": "/coding/model", 
    //             "page_ispublish": "0", 
    //             "page_type": "模块", 
    //             "page_ordernum": 1, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "7bcf84e014dc457e96a5dabd8e41a1ef", 
    //             "page_id": "7bcf84e014dc457e96a5dabd8e41a1ef", 
    //             "page_num": "MaxCodePage", 
    //             "page_parent": "2b290c31df96418fa9ed4d67f7a174af", 
    //             "parentId": "2b290c31df96418fa9ed4d67f7a174af", 
    //             "page_name": "页面生成", 
    //             "page_icon": "", 
    //             "page_url": "/coding/page", 
    //             "page_ispublish": "0", 
    //             "page_type": "模块", 
    //             "page_ordernum": 2, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "ad76e936eec74a7786c08eb10e6c4f4b", 
    //             "page_id": "ad76e936eec74a7786c08eb10e6c4f4b", 
    //             "page_num": "MaxCodeDefined", 
    //             "page_parent": "2b290c31df96418fa9ed4d67f7a174af", 
    //             "parentId": "2b290c31df96418fa9ed4d67f7a174af", 
    //             "page_name": "自定义模块", 
    //             "page_icon": "", 
    //             "page_url": "/coding/defined", 
    //             "page_ispublish": "0", 
    //             "page_type": "模块", 
    //             "page_ordernum": 3, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }, 
    //         {
    //             "id": "12ae2756ef014ab2aa57a7ff5ae7d016", 
    //             "page_id": "12ae2756ef014ab2aa57a7ff5ae7d016", 
    //             "page_num": "MaxFlow", 
    //             "page_parent": "2b290c31df96418fa9ed4d67f7a174af", 
    //             "parentId": "2b290c31df96418fa9ed4d67f7a174af", 
    //             "page_name": "流程设计器", 
    //             "page_icon": "", 
    //             "page_url": "/flow/designer", 
    //             "page_ispublish": "0", 
    //             "page_type": "模块", 
    //             "page_ordernum": 5, 
    //             "page_status": "1", 
    //             "children": [ ]
    //         }
    //     ]
    // }, 

]