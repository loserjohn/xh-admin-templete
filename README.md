<!--
 * @Descripttion:
 * @version:
 * @Author: loserjohn
 * @Date: 2020-09-27 14:35:19
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-05-08 14:20:13
-->

# vue-admin

vue-admin 重大项目监控管理

## 主要技术

基于 vue-admin 的二次开发，vue + router + vuex + cli3.x

---

## 安装教程

需要 node V14.x 最好

建议安装一个 nvm node 环境管理工具 安装教程 自行百度；

clone 项目到本地；

    npm i

安装依赖包，切记： **一定要使用 npm 安装 其他工具 回报乱七八糟的错误 本人亲测**；
若有网速原因 建议配置 淘宝 镜像源

    npm config set registry https://registry.npm.taobao.org

或者 ‘科学上网哈’；

方便使用 建议下载 vue cli ui 工具；

官网：
[](https://cli.vuejs.org/zh/guide/)

    npm install -g @vue/cli-service-global

git bash 里面使用：

    vue ui

可以使用 vue ui 可视化项目管理工具；

---

## 运行项目

### 开发环境

    npm run dev

**建议 使用 可视化工具直接运行 香~~~~~！**

### 生产环境

    build:prod

---

## 主要页结构分级

-views - login 用户登录类页面 - sys 系统设置页面

---

## 注意事项

- rbac 部分的接口可以查看

https://www.apizza.net/project/8b3df8e547994a420afc353aafe94efd/browse

-通用的增删改查 一般混在 utils/baseModal 里面，组件里面只需要自定义重写方法就好了

- 百度编辑参考
  http://fex.baidu.com/ueditor/#start-config

- tinymce 编辑器参考
  http://tinymce.ax-z.cn/quick-start.php

## npm 失败 常用处理方法

- 彻底删除本地 node_nodules 文件夹

  `npm install rimraf -g`  
   `rimraf node_modules`

- 部分 ssl 协议的 插件仓库克隆不了

  `git config --global http.sslVerify false`
  `git config --global url."https://github".insteadOf github:`

- 清除本地的 npm 缓存

  `npm cache clean--force`

- 配置镜像源

  `npm config set registry https://registry.npm.taobao.org`
