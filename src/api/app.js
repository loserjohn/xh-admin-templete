/*
 * @Author       : xh
 * @Date         : 2022-04-29 14:28:38
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-27 15:23:27
 * @FilePath     : \xh-admin-templete\src\api\app.js
 */
import request from '@/utils/request';

export function upload(data) {
  return request({
    url: '/upload', // 假地址 自行替换
    method: 'post',
    data
  });
}
