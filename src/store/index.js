/*
 * @Author: loserjohn
 * @Date: 2022-03-27 12:28:48
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-21 12:54:40
 * @Description: ---
 * @FilePath: \sys\src\store\index.js
 */
import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters";
// import bpmn from '@/components/bpmnModeler/store/modules/bpmn'

Vue.use(Vuex);

// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = require.context("./modules", false, /\.js$/);

// you do not need `import app from './modules/app'`
// it will auto require all vuex module from modules file
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  // set './app.js' => 'app'
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, "$1");
  const value = modulesFiles(modulePath);
  modules[moduleName] = value["default"];
  return modules;
}, {});

// modules['bpmn'] = bpmn

const store = new Vuex.Store({
  modules,
  getters
});

export default store;
