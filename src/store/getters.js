/*
 * @Descripttion:
 * @version:
 * @Author: loserjohn
 * @Date: 2020-09-27 14:35:22
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-03-26 00:19:04
 */
const getters = {
  sidebar: state => state.app.sidebar,
  language: state => state.app.language,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  menus: state => state.user.menus,
  permissions: state => state.user.permissions,
  unitcode: state => state.user.unitcode,
  is_admin: state => state.user.is_admin,
  sysMsgAccount: state => state.user.sysMsgAccount,
  introduction: state => state.user.introduction,
  roles: state => state.user.roles,
  permission_routes: state => state.permission.routes,
  errorLogs: state => state.errorLog.logs,
  systemKey: state => state.user.systemKey,
  refreshKey: state => state.app.refreshKey,
  collateOrder: state => state.collate.order
}
export default getters
