/*
 * @Author: loserjohn
 * @Date: 2022-03-26 00:15:56
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-03-26 00:18:38
 * @Description: ---
 * @FilePath: \cfpt-admin\src\store\modules\collate.js
 */
const state = {
  order: null
}

const mutations = {
  SET_ORDER: (state, payload) => {
    state.order = payload
  },
}

const actions = {

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
