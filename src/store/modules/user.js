import {
  login
} from '@/api/user'
import {
  resetRouter
} from '@/router'
import defaultSettings from '@/settings'
import {
  getLocalStorage, getMenus, getToken, removeLocalStorage, removeMenus, removeToken, setLocalStorage, setMenus, setToken
} from '@/utils/auth'
const {
  code
} = defaultSettings

const localUserInfo = getLocalStorage('userInfo')

const state = {
  token: getToken(),
  name: localUserInfo?localUserInfo.real_name:'',
  avatar: localUserInfo?localUserInfo.avatar_url:'',
  menus: getMenus(), // 存放角色的路由列表
  permisssions: [], // 用户色

  unitcode: '',
  is_admin: false,
  sysMsgAccount: 0,
  systemKey: 'CRM'
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_SysMsgAccount: (state, name) => {
    state.sysMsgAccount = name
  },
  SET_ROLE: (state, is_admin) => {
    state.is_admin = is_admin == 1
  },
  SET_UNIT: (state, unitcode) => {
    state.unitcode = unitcode
  },
  SET_AVATAR: (state, avatar) => {

    state.avatar = avatar
  },
  SET_SYS: (state, systemKey) => {
    state.systemKey = systemKey
  },
  SWITCH_SYS: (state, systemKey) => {
    state.name = ''
    state.avatar = ''
  },
  SET_MENUS: (state, menus) => {
    // debugger
    state.menus = menus
  },
  SET_PERMISSION: (state, permisssions) => {
    state.permisssions = permisssions
  }
}

const actions = {

  // user login
  sso({
    commit
  }, token) {
    // return new Promise((resolve, reject) => {
    //   commit('SET_TOKEN', token)
    //   setToken(token)
    //   resolve()
    // })
  },

  // user login
  login({
    commit
  }, userInfo) {
    const {
      username,
      password,
      code
      // systemKey
    } = userInfo
    return new Promise((resolve, reject) => {
      login({
        telephone: username.trim(),
        password: password,
        code: code
      }).then(response => {
        // debugger
        const {
          token = '', permisssion = [], menus = [], user
        } = response
        commit('SET_TOKEN', token)
        setToken(token)
        setMenus(menus)
        // user.roles.map(it=>it.name)
        setLocalStorage('userInfo',user)
        commit('SET_ROLE', user.roles)
        commit('SET_NAME', user.real_name)
        commit('SET_AVATAR', user.avatar_url)
        commit('SET_MENUS', menus)
        commit('SET_PERMISSION', permisssion)
        resolve()
      }).catch(error => {
        // debugger
        reject(error)
      })
    })
  },
  // get user info
  async getSysMsgAccount({
    commit,
    state
  }) {
    // const res = await request({
    //   url: '/Msg/SystemInfo/SystemInfoCount',
    //   method: 'get',
    // });
    // // let demo =  Math.random()
    // if (res.result === 1) {
    //   commit('SET_SysMsgAccount', res.data?res.data:0)
    // }
  },
  // get user info
  getInfo({
    commit,
    state
  }) {
    return new Promise((resolve, reject) => {
      const key = state.systemKey
      // debugger
      // getInfo(key).then(response => {
      //   const {
      //     data
      //   } = response

      //   if (!data) {
      //     reject('获取用户信息失败，请重新登录.')
      //   }

      //   const {
      //     name,
      //     avatar,
      //     is_admin
      //   } = data
      //   commit('SET_ROLE', is_admin)
      //   commit('SET_NAME', name)
      //   commit('SET_AVATAR', avatar)
      //   commit('SET_UNIT', data.userInfo.user_unit)

      //   resolve(data)
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // user logout
  logout({
    commit,
    state
  }) {
    return new Promise((resolve, reject) => {
      commit('SET_TOKEN', '')
      commit('SET_NAME', '')
      // commit('SET_SysMsgAccount', 0)
      removeToken()
      removeMenus()
      resetRouter()
      resolve()
    })
  },

  // remove token
  resetToken({
    commit
  }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      removeToken()
      removeMenus()
      removeLocalStorage('userInfo')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

const testMenus = [

  {
    'id': 1,
    'name': 'system',
    'display_name': '系统管理',
    'level': 0,
    'pid': 0,
    'sort': 1,
    'icon': 'international',
    'is_menu': 1,
    'mark': '',
    'status': 1,
    'updated_at': '2021-05-29T02:40:01.000000Z',
    'created_at': '2021-05-28T08:18:15.000000Z',
    'subs': [{
      'id': 2,
      'name': 'system_menus',
      'display_name': '菜单管理',
      'level': 1,
      'pid': 1,
      'sort': 2,
      'icon': 'international',
      'is_menu': 1,
      'mark': '',
      'status': 1,
      'updated_at': '2021-05-29T02:46:48.000000Z',
      'created_at': '2021-05-28T08:19:58.000000Z',
      'subs': []
    },
    {
      'id': 3,
      'name': 'system_roles',
      'display_name': '角色管理',
      'level': 1,
      'pid': 1,
      'sort': 1,
      'icon': 'international',
      'is_menu': 1,
      'mark': '',
      'status': 1,
      'updated_at': '2021-05-29T02:40:16.000000Z',
      'created_at': '2021-05-28T08:21:03.000000Z',
      'subs': []
    },
    {
      'id': 4,
      'name': 'system_auths',
      'display_name': '权限管理',
      'level': 1,
      'pid': 1,
      'sort': 1,
      'icon': 'international',
      'is_menu': 1,
      'mark': '',
      'status': 1,
      'updated_at': '2021-05-29T02:58:01.000000Z',
      'created_at': '2021-05-28T08:21:24.000000Z',
      'subs': []
    },
    {
      'id': 4,
      'name': 'system_roleright',
      'display_name': '菜单权限配置',
      'level': 1,
      'pid': 1,
      'sort': 1,
      'icon': 'international',
      'is_menu': 0,
      'mark': '',
      'status': 1,
      'updated_at': '2021-05-29T02:58:01.000000Z',
      'created_at': '2021-05-28T08:21:24.000000Z',
      'subs': []
    },
    {
      'id': 4,
      'name': 'system_users',
      'display_name': '用户管理',
      'level': 1,
      'pid': 1,
      'sort': 1,
      'icon': 'international',
      'is_menu': 1,
      'mark': '',
      'status': 1,
      'updated_at': '2021-05-29T02:58:01.000000Z',
      'created_at': '2021-05-28T08:21:24.000000Z',
      'subs': []
    }
    ]
  },
  {
    'id': 7,
    'name': 'products',
    'display_name': '产品管理',
    'level': 0,
    'pid': 0,
    'sort': 1,
    'icon': 'international',
    'is_menu': 1,
    'mark': '',
    'status': 1,
    'updated_at': '2021-05-29T03:44:12.000000Z',
    'created_at': '2021-05-29T03:44:12.000000Z',
    'subs': [{
      'id': 11,
      'name': 'products_manage',
      'display_name': '产品列表',
      'level': 1,
      'pid': 7,
      'sort': 500,
      'icon': 'international',
      'is_menu': 1,
      'mark': '',
      'status': 1,
      'updated_at': '2021-07-20T06:17:32.000000Z',
      'created_at': '2021-07-20T06:17:21.000000Z',
      'subs': []
    },
    {
      'id': 9,
      'name': 'products_class',
      'display_name': '产品分类',
      'level': 1,
      'pid': 7,
      'sort': 4,
      'icon': 'international',
      'is_menu': 1,
      'mark': '',
      'status': 1,
      'updated_at': '2021-05-29T03:46:12.000000Z',
      'created_at': '2021-05-29T03:46:12.000000Z',
      'subs': []
    },
    {
      'id': 10,
      'name': 'carModel',
      'display_name': '车型库配置',
      'level': 1,
      'pid': 7,
      'sort': 2,
      'icon': 'smile',
      'is_menu': 0,
      'mark': '',
      'status': 1,
      'updated_at': '2021-05-29T07:01:09.000000Z',
      'created_at': '2021-05-29T07:01:09.000000Z',
      'subs': []
    },
    {
      'id': 8,
      'name': 'recommend',
      'display_name': '推荐参数配置',
      'level': 1,
      'pid': 7,
      'sort': 1,
      'icon': 'setting',
      'is_menu': 0,
      'mark': '',
      'status': 1,
      'updated_at': '2021-05-29T03:45:24.000000Z',
      'created_at': '2021-05-29T03:45:24.000000Z',
      'subs': []
    }
    ]
  },
  {
    'id': 13,
    'name': 'crm',
    'display_name': '客户管理',
    'level': 0,
    'pid': 0,
    'sort': 1,
    'icon': 'international',
    'is_menu': 1,
    'mark': '',
    'status': 1,
    'updated_at': '2021-08-11T01:48:30.000000Z',
    'created_at': '2021-08-11T01:47:37.000000Z',
    'subs': [{
      'id': 14,
      'name': 'crm_agents',
      'display_name': '代理商管理',
      'level': 1,
      'pid': 13,
      'sort': 1,
      'icon': 'international',
      'is_menu': 1,
      'mark': '',
      'status': 1,
      'updated_at': '2021-08-12T07:24:57.000000Z',
      'created_at': '2021-08-11T01:49:16.000000Z',
      'subs': []
    },
    {
      'id': 15,
      'name': 'cityData',
      'display_name': '城市数据',
      'level': 1,
      'pid': 13,
      'sort': 1,
      'icon': '',
      'is_menu': 0,
      'mark': '',
      'status': 1,
      'updated_at': '2021-08-14T06:47:04.000000Z',
      'created_at': '2021-08-12T06:28:05.000000Z',
      'subs': []
    }
    ]
  }
]
