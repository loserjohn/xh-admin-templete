import Layout from '@/layout';
import { constantRoutes, routeComponent } from '@/router';
// import { off } from 'codemirror'

// console.log('routeComponent',routeComponent)
/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
// export function filterAsyncRoutes(menus) {
//   // 调整模块到耳机菜单下
//   const rootKeys = []
//   for (let index = 0; index < menus.length; index++) {
//     if (menus[index].page_parent === '') {
//       rootKeys.push(menus[index].page_id)
//     }
//   }

//   for (let index = 0; index < menus.length; index++) {
//     if (menus[index].page_type !== '1') {
//       for (let j = 0; j < menus.length; j++) {
//         if (menus[j].page_id === menus[index].page_parent && rootKeys.indexOf(menus[j].page_id) === -1) {
//           menus[index].page_parent = menus[j].page_parent
//           break
//         }
//       }
//     }
//   }

//   const res = []
//   menus.map(v => {
//     // 判断是否为根节点
//     if (v.page_parent === '') {
//       const t = filterSonRoutes(menus, v)
//       res.push(t)
//     }
//   })
//   // console.log(res)
//   return res
// }

// // 寻找某数组地下特定的路由
// function filterSonRoutes(routes, v) {
//   // let root_pid = item.page_id
//   // 找到 routes下所有page_parent ==  v.page_id的子项
//   const {
//     page_id,
//     page_url,
//     page_name,
//     page_num,
//     redirect,
//     page_icon,
//     page_type
//   } = v

//   const url_relations = page_url.split('>>')
//   const urls = url_relations.length > 1 ? url_relations[1].split('?') : url_relations[0].split('?')
//   const page_url_ = urls[0]
//   const metas = {}
//   if (urls.length > 1 && urls[1].length > 0) {
//     const params_items = urls[1].split('&')
//     for (let i = 0; i < params_items.length; i++) {
//       const items = params_items[i].split('=')
//       metas[items[0]] = items[1]
//     }
//   }
//   metas['id'] = page_id
//   metas['title'] = page_name
//   metas.icon = page_icon
//   const item = {
//     path: url_relations.length > 1 ? url_relations[0] : page_url_,
//     name: page_num,
//     redirect,
//     meta: metas,
//     children: [],
//     hidden: page_type !== '1'
//   }
//   if (v.page_parent === '') {
//     item.component = Layout
//   } else {
//     // const component_path = page_url_.split('/:')[0]
//     // component_path = `@/views${component_path}`
//     // component_path = '@/views' + component_path
//     // console.log(component_path)
//     // item.component = () => import(`@/views${page_url_.split('/:')[0]}`)
//     item.component = () => Promise.resolve(require(`@/views${page_url_.split('/:')[0]}`).default)
//     // item.component = () => import('@/views/account/user')
//     // component_path = `@/views${component_path}`
//     // item.component = () => import(component_path)
//     // console.log(item)
//   }
//   routes.map(u => {
//     if (u.page_parent === page_id) {
//       item.children.push(filterSonRoutes(routes, u))
//     }
//   })

//   return item
// }

const state = {
  routes: [],
  addRoutes: []
};

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes;
    state.routes = constantRoutes.concat(routes);
  },
  RESET_ROUTES: (state, routes) => {
    state.addRoutes = [];
    state.routes = [];
  }
};

const actions = {
  generateRoutes({
    commit
  }, menus) {
    return new Promise((resolve) => {
      console.log('menus', menus);
      const accessedRoutes = filterAsyncRoutes(menus);

      accessedRoutes.push({ path: '*', redirect: '/404', hidden: true });

      console.log('accessedRoutes', accessedRoutes);

      commit('SET_ROUTES', accessedRoutes);
      resolve(accessedRoutes);
    });
  },
  reset({
    commit
  }) {
    commit('RESET_ROUTES');
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};


/**
 *
 *
 * @export
 * @param {*} menus
 * @return {*}
 */
export function filterAsyncRoutes(menus) {
  const res = [];
  menus.map((it) => {
    // debugger
    const metas = {};
    metas.id = it.id;
    metas.title = it.display_name;
    metas.icon = it.icon;
    metas.loadOnCache = it.loadOnCache;
    metas.leaveOffCache = it.leaveOffCache;
    const item = {
      path: it.name,
      name: it.name,
      redirect: '',
      meta: metas,
      hidden: it.status != '1' || it.is_menu != '1'
    };

    // item.component = () => Promise.resolve(require(`@/views${it.page_url.split('/:')[0]}`).default)
    item.component = Layout;
    // 多级菜单
    if (it.subs && it.subs.length > 0) {

      item.children = filterSon(it.subs);
      res.push(item);
    } else if (routeComponent[item.name]) {
      // 单级菜单

      item.children = [{
        ...item,
        component: routeComponent[item.name].component
      }];
      item.path = routeComponent[item.name].path + '_';
      res.push(item);
    }


  });
  return res;
}

/**
 *
 *
 * @export
 * @param {*} menus
 * @return {*}
 */
export function filterSon(menus) {
  const res = [];
  menus.map((it) => {
    const metas = {};

    metas.id = it.id;
    metas.title = it.display_name;
    metas.icon = it.icon;
    metas.loadOnCache = it.loadOnCache;
    metas.leaveOffCache = it.leaveOffCache;
    const item = {
      // path: it.page_url,
      name: it.name,
      redirect: '',
      // meta: metas,
      hidden: it.status != '1' || it.is_menu != '1'
    };
    if (routeComponent[it.name]) {
      // debugger
      const { path } = routeComponent[it.name];
      // debugger
      const urls = path.split('?');
      if (urls.length > 1 && urls[1].length > 0) {
        const params_items = urls[1].split('&');
        for (let i = 0; i < params_items.length; i++) {
          const items = params_items[i].split('=');
          metas[items[0]] = items[1];
        }
      }

      // item.component = () => Promise.resolve(require(`@/views${it.page_url.split('/:')[0]}`).default)
      item.component = routeComponent[it.name].component;
      item.path = path;
      item.meta = metas;
    } else {
      item.path = it.name;
      item.meta = metas;
      item.component = () => import('@/views/error-page/404');
    }
    if (it.subs && it.subs.length > 0) {
      item.children = filterSon(it.subs);
    }

    res.push(item);
  });
  return res;
}
