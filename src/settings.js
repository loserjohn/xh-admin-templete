/*
 * @Descripttion:
 * @version:
 * @Author: loserjohn
 * @Date: 2020-09-27 14:35:22
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-05-08 12:27:17
 */
module.exports = {
  // code: 'MAX',
  code: 'GJL',
  title: 'xh_templete',

  /**
   * @type {boolean} true | false
   * @description Whether show the settings right-panel
   */
  authOnlyCurrentCode: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the settings right-panel
   */
  showSettings: true,

  /**
   * @type {boolean} true | false
   * @description Whether need tagsView
   */
  tagsView: true,

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: true,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true,

  /**
   * @type {string | array} 'production' | ['production', 'development']
   * @description Need show err logs component.
   * The default is only used in the production env
   * If you want to also use it in dev, you can pass ['production', 'development']
   */
  errorLog: 'production'
};
