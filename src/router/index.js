/* Layout */
import Layout from '@/layout';
import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);


/* Router Modules */
// import componentsRouter from './modules/components'
// import chartsRouter from './modules/charts'
// import tableRouter from './modules/table'
// import nestedRouter from './modules/nested'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: false                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * routeComponent
 * 前端控制 路由表唯一id  对应的本地组件 和 跳转路由
 * all roles can be accessed
 */
export const routeComponent = {

  /** ************系统部分*****************/
  system_menus: {
    path: '/sys/page',
    component: () => import('@/views/sys/page')
  },
  system_roles: {
    path: '/sys/role',
    component: () => import('@/views/sys/role')
  },
  system_auths: {
    path: '/sys/auths',
    component: () => import('@/views/sys/auths')
  },
  system_roleright: {
    path: '/sys/roleright/:id/:name',
    component: () => import('@/views/sys/roleright')
  },
  system_users: {
    path: '/sys/users',
    component: () => import('@/views/sys/users')
  },
  system_admin_user: {
    path: '/sys/admins',
    component: () => import('@/views/sys/admins')
  },
  system_dict: {
    path: '/sys/dict',
    component: () => import('@/views/sys/dict')
  }

};

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
  path: '/redirect',
  component: Layout,
  hidden: true,
  children: [{
    path: '/redirect/:path*',
    component: () => import('@/views/redirect/index')
  }]
},
{
  path: '/login',
  component: () => import('@/views/login/index'),
  hidden: true
},
{
  path: '/auth-redirect',
  component: () => import('@/views/login/auth-redirect'),
  hidden: true
},
// {
//   path: '/dashboard',
//   component: () => import('@/views/dashboard/index'),
//   name: 'Dashboard',
//   meta: {
//     title: '主页',
//     icon: 'dashboard',
//     affix: false
//   }
// },
{
  path: '',
  component: Layout,
  redirect: 'dashboard',
  children: [
    {
      path: '/dashboard',
      component: () => import('@/views/dashboard/index'),
      name: 'Dashboard',
      meta: {
        title: '主页',
        icon: 'dashboard',
        affix: true,
        'loadOnCache': 1,
        'leaveOffCache': 0
      }
    },
    {
      path: '/account/password',
      component: () => import('@/views/account/password'),
      name: 'Password',
      meta: {
        title: '密码编辑',
        icon: 'dashboard',
        'loadOnCache': 1,
        'leaveOffCache': 0
      },
      hidden: true
    }]
},
{
  path: '/example',
  component: Layout,
  meta: {
    title: '组件示例',
    icon: 'dashboard'
  },
  children: [
    {
      path: '/example/upload',
      component: () => import('@/views/example/upload'),
      name: 'Upload',
      meta: {
        title: '上传',
        icon: 'dashboard',
        affix: false,
        'loadOnCache': 1,
        'leaveOffCache': 0
      }
    },
    {
      path: '/example/editor',
      component: () => import('@/views/example/editor'),
      name: 'Editor',
      meta: {
        title: '富文本编辑',
        icon: 'dashboard',
        'loadOnCache': 1,
        'leaveOffCache': 1
      }
    }]
},
{
  path: '/401',
  component: () => import('@/views/error-page/401'),
  hidden: true
},
{
  path: '/404',
  component: () => import('@/views/error-page/404'),
  hidden: true
}
  // {
  //   path: '',
  //   redirect: '/login',
  // }
];


const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
});

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
