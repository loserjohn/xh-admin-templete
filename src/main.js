/*
 * @Descripttion:
 * @version:
 * @Author: loserjohn
 * @Date: 2020-09-30 14:09:19
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-05-08 12:25:17
 */
import '@/styles/index.scss'; // global css
import request from '@/utils/request';
// import uploader from 'vue-simple-uploader'
// Vue.use(uploader)
/**
 * If you don't want to use mock-server
 * you want to use mockjs for request interception
 * you can execute:
 *
 * import { mockXHR } from '../mock'
 * mockXHR()
 */
// import  'animate.css'
import animated from 'animate.css'; // npm install animate.css --save安装，在引入
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/display.css';
import Cookies from 'js-cookie';
import 'normalize.css/normalize.css'; // a modern alternat
import Vue from 'vue';
import App from './App';
import './icons'; // icon
import i18n from './lang'; // internationalization
import './permission'; // permission control
import router from './router';
import store from './store';
import './styles/element-variables.scss';
import './utils/error-log'; // error log


// Vue.prototype.$echartsGL = echartsGL

if (!Cookies.get('size')) {
  Cookies.set('size', 'mini');
}

Vue.use(animated);
Vue.use(Element, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value)
});

const initDict = async () => {
  try {
    const res = await request.get('/api/dicts');
    console.log('dict', res);
    localStorage.setItem('dict', JSON.stringify(res));

    Object.keys(res).forEach((key) => {
      // console.log(1,res[key] instanceof Array )
      if (res[key] instanceof Array) {
        // console.log(2,res[key],key)
        Vue.filter(`${key}_filter`, function (val) {
          // console.log(3,val)
          const arr = res[key];
          let restext = '--';
          if (!val && val != 0) {return restext;}
          arr.some((it) => {
            // console.log(4,it)
            if (it.id == val) {
              restext = it.label;
            }
          });
          return restext;
        });
      }
    });
  } catch (error) {

  }
};
// 全局字段项
initDict();
// register global utility filters
// Object.keys(filters).forEach(key => {
//   Vue.filter(key, filters[key])
// })

Vue.config.productionTip = false;

export default new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: (h) => h(App)
});
