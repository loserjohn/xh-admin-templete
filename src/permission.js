/*
 * @Author       : xh
 * @Date         : 2022-04-29 14:28:39
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-27 18:32:01
 * @FilePath     : \xh-admin-templete\src\permission.js
 */
/*
 * @Descripttion:
 * @version:
 * @Author: loserjohn
 * @Date: 2020-09-27 14:35:22
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-03-18 21:57:54
 */
import { getMenus, getToken } from '@/utils/auth'; // get token from cookie
import getPageTitle from '@/utils/get-page-title';
import { Message } from 'element-ui';
import NProgress from 'nprogress'; // progress bar
import 'nprogress/nprogress.css'; // progress bar style
import router, { constantRoutes, resetRouter } from './router';
import store from './store';
let a = false;
NProgress.configure({ showSpinner: false }); // NProgress Configuration

const whiteList = ['/login', '/auth-redirect']; // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start();

  // set page title
  document.title = getPageTitle(to.meta.title);

  // determine whether the user has logged in
  const hasToken = getToken();

  // console.log('to', to, from);
  const menus = getMenus();
  // debugger
  if (hasToken && menus) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({ path: '/' });
      NProgress.done();
    } else {
      // determine whether the user has obtained his permission roles through getInfo
      const hasUser = store.getters.permission_routes && store.getters.permission_routes.length > 0;

      // debugger
      if (hasUser) {
        // //debugger
        // await store.dispatch('tagsView/addCachedView', to);
        store.dispatch('tagsView/delCachedView', from);

        next();
      } else {
        // debugger
        try {
          const accessRoutes = await store.dispatch('permission/generateRoutes', menus);

          // dynamically add accessible routes
          resetRouter();
          const integralRoutes = constantRoutes.concat(accessRoutes);
          console.log('integralRoutes', accessRoutes, integralRoutes);
          router.options.routes = integralRoutes;


          router.addRoutes(accessRoutes);

          console.log('router', router);
          // hack method to ensure that addRoutes is complete
          // set the replace: true, so the navigation will not leave a history record
          a = true;
          next({ ...to, replace: true });
        } catch (error) {
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken');
          Message.error(error || '发生错误');
          next(`/login?redirect=${to.path}`);
          NProgress.done();
        }
      }
    }
  } else {

    /* has no token*/
    // debugger
    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next();
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`);
      NProgress.done();
    }
  }
});

router.afterEach(() => {
  // finish progress bar
  NProgress.done();
});

