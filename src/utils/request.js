/*
 * @Descripttion:
 * @version:
 * @ Modified by: loserjohn
 * @ Modified time: 2021-12-01 21:31:52
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-03-23 11:52:35
 */
import store from '@/store'
import {
  getToken
} from '@/utils/auth'
import axios from 'axios'
import { Loading, Message } from 'element-ui'
let loadingInstance =null
let requstQueue = 0

const  options = {
  body:true,
  lock: true,
  text: 'Loading',
  spinner: 'el-icon-loading',
  background: 'rgba(0, 0, 0, 0)'
}

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  withCredentials: false, // send cookies when cross-domain requests
  timeout: 30000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    if (store.getters.token) {
      // let each request carry token --['X-Token'] as a custom key.
      // please modify it according to the actual situation.
      config.headers['Authorization'] = getToken()
    } 
    showLoading()
    return config
  },
  error => {
    // do something with request error
   
    hideLoading()
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code.
   */
  response => {
    const res = response.data 
    // debugger
    hideLoading()
    if (response.status >= 200 && response.status < 300) {
      return res
    } else {
      Message({
        message: JSON.stringify(res),
        type: 'error',
        duration: 5 * 1000
      })
      // return res
      return Promise.reject(res)
    }
    // if (res.status ===  401) {
    //   // to re-login
    //   MessageBox.confirm('身份过期，请重新登录。', '确认退出', {
    //     confirmButtonText: '重新登陆',
    //     cancelButtonText: '取消',
    //     type: 'warning'
    //   }).then(() => {
    //     store.dispatch('user/resetToken').then(() => {
    //       location.reload()
    //     })
    //   })
    // }else{

    // }
  },
  (error) => { 
    hideLoading();
    
    console.log('err：', error.response) // for debug
    const err = error.response.data || null
    // Message({
    //   message: err && err.message ? err.message : err.request_url,
    //   type: 'error',
    //   duration: 5 * 1000
    // })
    // if (error.message.indexOf('401') >= 0) {
    //   router.push({
    //     path: '/401'
    //   })
    // }

    return Promise.reject(err)
  }
)

function showLoading(){
  
  if(requstQueue<=0){
    console.log('loading',requstQueue)
    loadingInstance = Loading.service(options);
    requstQueue = 0
  } 
  requstQueue+=1
}

function hideLoading(){
  requstQueue-=1
  if(requstQueue<=0) {
    console.log('hideLoading',requstQueue)
    loadingInstance.close();
    loadingInstance = null;
    requstQueue = 0
  }
 
}

export default service
