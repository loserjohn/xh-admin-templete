/*
 * @Author: loserjohn
 * @Date: 2021-11-30 21:30:46
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-03-23 11:27:39
 * @Description: ---
 * @FilePath: \cfpt-admin\src\utils\auth.js
 */
import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const MenusKey = 'Admin-Menus'
export function getToken() {
  const c = Cookies.get(TokenKey)
  return c ? `Bearer ${c}` : null
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getMenus() {
  const str = localStorage.getItem(MenusKey)
  return str ? JSON.parse(str) : null
}

export function setMenus(m) {
  if (!m) return
  const str = JSON.stringify(m)
  return localStorage.setItem(MenusKey, str)
}

export function removeMenus() {
  return localStorage.removeItem(MenusKey)
}



export function getLocalStorage(m) {
  if (!m) return
  const str = localStorage.getItem(m)
  return str ? JSON.parse(str) : null
}
export function setLocalStorage(m,v) {
  if (!m ||  !v) return
  const str = JSON.stringify(v)
  return localStorage.setItem(m, str)
}

export function removeLocalStorage(m) {
  if (!m) return
  return localStorage.removeItem(m)
}
