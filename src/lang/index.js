/*
 * @Descripttion:
 * @version:
 * @Author: loserjohn
 * @Date: 2020-09-27 14:35:22
 * @LastEditors: loserjohn
 * @LastEditTime: 2020-10-23 23:32:59
 */
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookies from 'js-cookie'
import elementEnLocale from 'element-ui/lib/locale/lang/en' // element-ui lang
import elementZhLocale from 'element-ui/lib/locale/lang/zh-CN'// element-ui lang
import elementEsLocale from 'element-ui/lib/locale/lang/es'// element-ui lang
import enLocale from './en'
import zhLocale from './zh'
import esLocale from './es'
// import { getLocale } from 'vue-cron-generator/src/util/tools'
import enUSLocale from 'vue-cron-generator/src/locale/en-US'
import zhCNLocale from 'vue-cron-generator/src/locale/zh-CN'

Vue.use(VueI18n)

const messages = {
  en: {
    ...enLocale,
    ...elementEnLocale,
    ...enUSLocale
  },
  zh: {
    ...zhLocale,
    ...elementZhLocale,
    ...zhCNLocale
  },
  es: {
    ...esLocale,
    ...elementEsLocale
  }
}
export function getLanguage() {
  const chooseLanguage = Cookies.get('language')
  if (chooseLanguage) return chooseLanguage

  // if has not choose language
  const language = (navigator.language || navigator.browserLanguage).toLowerCase()
  const locales = Object.keys(messages)
  for (const locale of locales) {
    if (language.indexOf(locale) > -1) {
      return locale
    }
  }
  return 'zh'
}
const i18n = new VueI18n({
  // set locale
  // options: en | zh | es
  locale: getLanguage(),
  // locale: getLocale(),
  // set locale messages
  messages,
  silentTranslationWarn: true
})

export default i18n
